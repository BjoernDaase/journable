# window.py
#
# Copyright 2020 Björn Daase
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from os import environ, path, makedirs
from json import loads, dumps
from gi.repository import Gtk, Gdk

from .utils import find_child

from .clear_confirmation_dialog import ClearConfirmationDialog
from .exit_confirmation_dialog import ExitConfirmationDialog
from .unsaved_changes_confirmation_dialog import UnsavedChangesConfirmationDialog


@Gtk.Template(resource_path='/net/daase/journable/ui/window.ui')
class JournableWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'JournableWindow'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.ensure_paths()
        self.get_ui_widget_handlers()
        self.load_file_from_last_session_if_exists()

    def ensure_paths(self):
        config_path = environ.get('XDG_CONFIG_PATH', path.join(
            environ['HOME'], '.config', 'journable'))
        makedirs(config_path, exist_ok=True)
        self.journal_history_path = path.join(config_path, 'file_history')
        if not path.exists(self.journal_history_path):
            journal_history_file = open(self.journal_history_path, 'w')
            journal_history_file.close()

    def get_ui_widget_handlers(self):
        self.header_bar = find_child(self, 'header_bar')
        self.day_text_views = [find_child(self, day) for day in [
            'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'weekend']]
        self.month_text_views = [find_child(self, month) for month in [
            'this_month', 'next_month']]

    def load_file_from_last_session_if_exists(self):
        journal_history_file = open(self.journal_history_path, 'r')
        journal_file_path = journal_history_file.read()
        if journal_file_path and path.isfile(journal_file_path):
            self.update_file_information(journal_file_path)
            self.load_file_content(journal_file_path)
            self.update_file_information(journal_file_path)
        else:
            self.set_default_file_information()

    def set_default_file_information(self):
        self.is_unsaved_file = True
        self.has_unsaved_changes = False
        self.current_journal_path = None
        self.current_journal_basename = 'Untitled'
        self.update_header_bar_information()

    def update_file_information(self, journal_file_path):
        self.is_unsaved_file = False
        self.has_unsaved_changes = False
        self.current_journal_path = journal_file_path
        self.current_journal_basename = path.splitext(
            path.basename(journal_file_path))[0]
        self.update_header_bar_information()

    def update_header_bar_information(self):
        if self.has_unsaved_changes:
            self.header_bar.set_title('*' + self.current_journal_basename)
        else:
            self.header_bar.set_title(self.current_journal_basename)

        self.header_bar.set_subtitle(self.current_journal_path)

    def update_file_history_information(self, journal_file_path):
        journal_history_file = open(self.journal_history_path, 'w')
        journal_history_file.write(journal_file_path)
        journal_history_file.close()

    def add_filters(self, file_chooser):
        filter_json = Gtk.FileFilter()
        filter_json.set_name('JSON files')
        filter_json.add_mime_type('application/json')
        file_chooser.add_filter(filter_json)

        filter_all = Gtk.FileFilter()
        filter_all.set_name('All files')
        filter_all.add_pattern('*')
        file_chooser.add_filter(filter_all)

    def load_file_content(self, filename):
        file = open(filename, 'r')
        data = loads(file.read())
        file.close()

        days = data['days']
        for day in days:
            text_view = list(
                filter(
                    lambda day_text_view: day_text_view.get_name() == day,
                    self.day_text_views))[0]
            buffer = text_view.get_buffer()
            buffer.begin_not_undoable_action()
            buffer.set_text(days[day], -1)
            buffer.end_not_undoable_action()

        months = data['months']
        for month in months:
            text_view = list(
                filter(
                    lambda month_text_view: month_text_view.get_name() == month,
                    self.month_text_views))[0]
            buffer = text_view.get_buffer()
            buffer.set_text(months[month], -1)

    def save_file_content(self, filename):
        data = {
            'days': {},
            'months': {}
        }

        for day in self.day_text_views:
            buffer = day.get_buffer()
            start_iter, end_iter = buffer.get_bounds()
            text = buffer.get_text(start_iter, end_iter, False)
            data['days'].update({day.get_name(): text})

        for month in self.month_text_views:
            buffer = month.get_buffer()
            start_iter, end_iter = buffer.get_bounds()
            text = buffer.get_text(start_iter, end_iter, False)
            data['months'].update({month.get_name(): text})

        file = open(filename, 'w')
        file.write(dumps(data, indent=4))
        file.close()

    def let_user_save_changes(self):
        if self.has_unsaved_changes:
            unsaved_changes_confirmation_dialog = UnsavedChangesConfirmationDialog(
                transient_for=self, modal=True)
            response = unsaved_changes_confirmation_dialog.run()
            unsaved_changes_confirmation_dialog.destroy()

            # Make sure to not clear the content if the saving was aborted at
            # any point. Therefore, just return if that is the case
            if response == Gtk.ResponseType.CANCEL:
                return
            elif response == Gtk.ResponseType.ACCEPT:
                file_chooser_dialog_response = self.save_file()
                if file_chooser_dialog_response == Gtk.ResponseType.CANCEL:
                    return

            self.has_unsaved_changes = False

    def clear_text_view_buffers(self):
        for day_text_view in self.day_text_views:
            buffer = day_text_view.get_buffer()
            buffer.set_text('', -1)

        for month_text_view in self.month_text_views:
            buffer = month_text_view.get_buffer()
            buffer.set_text('', -1)

    def do_delete(self):
        if not self.has_unsaved_changes:
            return False

        exit_confirmation_dialog = ExitConfirmationDialog(
            transient_for=self, modal=True)
        response = exit_confirmation_dialog.run()
        exit_confirmation_dialog.destroy()

        if response == Gtk.ResponseType.CLOSE:
            return False
        elif response == Gtk.ResponseType.ACCEPT:
            self.save_file()
            return False

        return True

    def text_changed(self):
        self.has_unsaved_changes = True
        self.update_header_bar_information()

    def open_file(self):
        if self.has_unsaved_changes:
            self.let_user_save_changes()

        if not self.has_unsaved_changes:
            file_chooser = Gtk.FileChooserNative.new(
                'Choose a file',
                self,
                Gtk.FileChooserAction.OPEN,
                '_Open',
                '_Cancel'
            )
            self.add_filters(file_chooser)
            response = file_chooser.run()

            if response == Gtk.ResponseType.ACCEPT:
                filename = file_chooser.get_filename()
                self.load_file_content(filename)
                self.update_file_information(filename)
                self.update_file_history_information(filename)

            file_chooser.destroy()

    def save_file_as(self):
        file_chooser = Gtk.FileChooserNative.new(
                'Save as',
                self,
                Gtk.FileChooserAction.SAVE,
                '_Save',
                '_cancel'
            )
        self.add_filters(file_chooser)
        response = file_chooser.run()
        if response == Gtk.ResponseType.ACCEPT:
            filename = file_chooser.get_filename()
            if not filename.endswith('.json'):
                filename += '.json'

            self.update_file_information(filename)
            self.save_file_content(self.current_journal_path)
            self.update_file_history_information(self.current_journal_path)

        file_chooser.destroy()


    def save_file(self):
        if self.is_unsaved_file:
            self.save_file_as()
        else:
            self.save_file_content(self.current_journal_path)
            self.update_file_information(self.current_journal_path)

    def new_journal(self):
        if self.has_unsaved_changes:
            self.let_user_save_changes()

        if not self.has_unsaved_changes:
            self.clear_text_view_buffers()
            self.set_default_file_information()

    def clear_journal(self):
        clear_confirmation_dialog = ClearConfirmationDialog(
            transient_for=self, modal=True)
        response = clear_confirmation_dialog.run()
        clear_confirmation_dialog.destroy()

        if response == Gtk.ResponseType.YES:
            self.clear_text_view_buffers()

    ### Callbacks from the UI ###

    def do_delete_event(self, event):
        return self.do_delete()

    @Gtk.Template.Callback()
    def text_changed_event(self, event):
        return self.text_changed()

    @Gtk.Template.Callback()
    def open_file_event(self, event):
        return self.open_file()

    @Gtk.Template.Callback()
    def save_file_event(self, event):
        return self.save_file()

    @Gtk.Template.Callback()
    def new_journal_event(self, event):
        return self.new_journal()

    @Gtk.Template.Callback()
    def clear_journal_event(self, event):
        return self.clear_journal()
