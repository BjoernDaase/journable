<a href="https://flathub.org/apps/details/net.daase.journable">
    <img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" />
</a>

<img height="32" src="data/icons/hicolor/scalable/apps/net.daase.journable.svg" /> Journable
=======================================================================================

A simple bullet journal

<img src="data/screenshots/application_window.png" />

Installation & Licensing
------------------------

Please consult the [INSTALL](./INSTALL.md) and [COPYING](./COPYING) files
for more information.
